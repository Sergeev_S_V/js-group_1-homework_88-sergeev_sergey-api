const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PostSchema = new Schema({
  author: {
    type: String, required: true
  },
  userId: {
    type: String, required: true
  },
  title: {
    type: String, required: true
  },
  description: String,
  image: String,
  datetime: {
    type: String, required: true
  }
});

const Post = mongoose.model('Post', PostSchema);

module.exports = Post;